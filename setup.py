#!/usr/bin/env python3

import setuptools

setuptools.setup(
      name="usertools",
      version="2.0.0",
      author="Jakim Friant",
      author_email="jfriant@cfcc.edu",
      url="https://www.cfcc.edu/share",
      description="User creation/modification utilities used by coluser.",
      py_modules=['usertools'],
      python_requires=">=3.6",
      entry_points={
            'console_scripts': [
                  'coluser = coluser:main',
            ]
      }
)
