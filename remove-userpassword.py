#!/usr/bin/env python
"""Remove the Unix password and expiration settings from a user account"""

# IMPORTANT: this must be up to date for your site
EXCLUDED_USERS = [
    'datatel',
    'eris',
    'falink',
    'flnk',
    'maestro',
    'ncmentor',
    'informer',
    'webwiz'
    ]

from optparse import OptionParser
import os
import sys

# NOTE: this is just for portable installations, don't create it if
#       you installed the required modules system-wide.  We'll look
#       for a site-packages location and a simple 'lib' directory.
ver_str = "python%d.%d" % (sys.version_info[0], sys.version_info[1])
for lib_dir in [os.path.join('.', 'lib', ver_str, 'site-packages'),
                os.path.join('.', 'lib')]:
    if os.access(lib_dir, os.F_OK):
        sys.path.append(lib_dir)

import usertools

USAGE = "%prog [options] [IMPORT_FILENAME=/etc/passwd]"

PASSWD_SEP = ":"
USERNAME_POS = 0
GID_POS = 3


def main():
    
    parser = OptionParser(usage=USAGE)
    parser.add_option('-d', '--debug', dest="debug",
                      action="store_true", default=False,
                      help="Debug mode, no updates made to the accounts")
    parser.add_option('-g', '--group', dest="group_id",
                      metavar='GID', default='100',
                      help="The GID of the group to which the users belong")

    (options, args) = parser.parse_args()

    if len(args) > 0:
        import_fn = args[0]
    else:
        usr_ans = input("Convert all users in /etc/passwd [y/N]? ")
        if usr_ans.lower() != 'y':
            parser.error("Please specify a file on the command line")
        import_fn = "/etc/passwd"

    print("[INFO] reading users from", import_fn)

    fd = open(os.path.expanduser(import_fn))

    print("")
    print("[STATUS] converting user accounts...")

    for line in fd:
        columns = line.split(PASSWD_SEP)
        username = columns[USERNAME_POS]
        group_id = columns[GID_POS]

        if group_id == options.group_id and username not in EXCLUDED_USERS:
            if not options.debug:
                usertools.delete_password(username)
            else:
                print("[DEBUG] would remove password settings from", username)


if __name__ == "__main__":
    main()
