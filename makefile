.PHONY: install clean proper dist import

DEST = /usr/local/sbin
PYTHON = /usr/bin/python

install:
	su root -c 'umask 022; cp coluser.py $(DEST)/coluser ; $(PYTHON) setup.py install ; rm -rf build'

clean:
	rmb
	rm -f *.py[oc]
	-rm MANIFEST

proper: clean
	rm -rf dist

dist:
	python setup.py sdist
