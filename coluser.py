#!/opt/usertools/venv/bin/python
"""Automate the creation of new Unix/Colleague user accounts

This is the coluser shell script rewritten in python to take advantage
of the pexpect module's advanced scripting abilities -- especially to
automate setting the password.

"""
__cvsid__ = "$Id$"
__version__ = "2.1"
__author__ = "Jakim Friant <jfriant@cfcc.edu>"

import datetime
from getpass import getpass
import logging
import logging.config
from optparse import OptionParser
import os
import sys

import faulthandler
faulthandler.enable()

from console import console

import pexpect

import intercall

import usertools

UDT_PASSWORD = ""
UDT_USER = ""

# List of preference/security files in colleague that contain the username
#
# NOTE: we are not renaming records in GLROLES since that would
#       require editing the GLUSERS record and renaming the keys in
#       the GLUS.ROLE.IDS field.
COLLEAGUE_PREFS = [
    'GLUSERS',
    'USER.FAVORITES',
    'USER.PREFERENCES',
    ]


def main():
    """Parse the command-line options.

    Now that there are two main functions (create new user, change a
    name) they have been split off into separate routines.

    """
    global UDT_USER
    global UDT_PASSWORD

    usage = "%prog [options] [<full> <name>]"

    epilog = "The default function is to create a new user for the Colleague system.  Selecting the 'change' option will switch to the second mode where only the information for that user is changed in Colleague."
    
    parser = OptionParser(usage=usage, epilog=epilog)
    parser.add_option('-c', '--change', dest="old_username", metavar="OLD_USERNAME",
                      help="Change the the user's login name (use with -e or -u)")
    parser.add_option('--colleague-only', dest='colleague_only',
                      action="store_true", default=False,
                      help="Rename the Colleague records only.")
    parser.add_option('--copy-to', dest='copy_to',
                      metavar='ENVIRONMENT', default=None,
                      help="Copy the OPERS record to the given environment")
    parser.add_option('-e', '--employee', dest="employee_number", default=None,
                      metavar="PERSON_ID",
                      help="Select the user by the Employee Number")
    parser.add_option('-F', '--ftp', dest="allow_ftp",
                      action="store_true", default=False,
                      help="Allow FTP access for the given user")
    parser.add_option('-g', dest="printer_groups",
                      default=None, metavar="GROUPS",
                      help="specify the printer GROUPS, comma delimited")
    parser.add_option('-l', '--log', dest="log",
                      action="store_true", default=False,
                      help="Write detailed log messages to a file.")
    parser.add_option('--no-ldap', dest="use_ldap",
                      action="store_false", default=True,
                      help="disable LDAP usernames and use the 8 character name")
    parser.add_option('--randpw', dest="rand_password",
                      action="store_true", default=True,
                      help="Use the new random password")
    parser.add_option('-E', '--env', dest="db_environment",
                      default='production', metavar="NAME",
                      help="Specify a DB environment (default: production)")
    parser.add_option('-u', '--username', dest="username",
                      default=None, metavar="NAME",
                      help="set the username to NAME")

    (options, args) = parser.parse_args()

    if options.db_environment not in usertools.DATABASE_PATHS.keys():
        parser.error("DB environment must be one of " + repr(usertools.DATABASE_PATHS.keys()) + " and not " + options.db_environment)

    #
    # Initialize logging if requested
    #
    if options.log:
        logging.config.fileConfig('logging.conf')
    #
    # Set up the user object with the user's name
    #
    if options.old_username or options.allow_ftp:
        usertools.in_modify_mode()
    elif options.copy_to is not None:
        usertools.in_modify_mode(2)

    usertools.cs()
    # print(usertools.WELCOME)
    # usertools.cs(wait_before=True)

    # Check to see if we have the correct LOCALE setting
    if os.getenv('LANG') != 'C.UTF-8':
        print("Please run 'export LANG=C.UTF-8' before running this script.")
        print("")
        sys.exit(1)

    #
    # Establish a valid password to Colleague
    #
    UDT_USER = console.input("Enter your [b]UniData[/b] username: ")
    UDT_PASSWORD = verify_password(UDT_USER)

    udt_session = usertools.get_session(UDT_USER, UDT_PASSWORD, options.db_environment, usertools.DATABASE_SERVER)

    #
    # Get the user's name and create an object for later use
    #
    user = usertools.User()

    # prompt for a username on a new employee account
    if options.employee_number is None and not options.allow_ftp and options.old_username is None:
        if len(args) > 0:
            fullname = " ".join(args)
        else:
            console.print("Enter the user's full name in mixed case seperated by spaces")
            fullname = console.input("(ex: First Last): ")

        user.parse(fullname)
        # even though we've established the username, the user might
        # still want to specify an alternate that we'll use instead.
        if options.username is not None:
            user.set_username(options.username)
    else:
        # using the global password to make the connection
        user.get_name_from_database(options.employee_number,
                                    options.use_ldap,
                                    udt_session)
        # allow the user to override the default username
        if options.username is not None:
            user.set_username(options.username)

    #
    # Now perform the requested action
    #
    if options.copy_to is not None:
        usertools.cs()
        dest_session = usertools.get_session(UDT_USER, UDT_PASSWORD, options.copy_to, usertools.DATABASE_SERVER)
        console.print("Copying %s from %s to %s" % (user.opersname(), options.db_environment, options.copy_to))
        success = usertools.copy_opers(user.opersname(),
                                       udt_session,
                                       dest_session)
        if success:
            console.print()
            console.rule("Done")
            console.print()
        else:
            console.print("[bold yellow]WARNING[/bold yellow]: copy failed!")
        try:
            dest_session.close()
        except intercall.IntercallError as e:
            console.print("[bold red]Error[/bold red] closing session: " + str(e))
    elif options.old_username:
        if options.colleague_only:
            change_colleague(UDT_USER, UDT_PASSWORD, options.db_environment, usertools.DATABASE_SERVER, options.old_username, user, options.log)
        else:
            success = change_name(udt_session, options.old_username, user, options.use_ldap, options.log)
            if success:
                change_colleague(UDT_USER, UDT_PASSWORD, options.db_environment, usertools.DATABASE_SERVER, options.old_username, user, options.log)
    else:
        create_user(user, options)

    # done, close the unidata session
    try:
        udt_session.close()
    except intercall.IntercallError as e:
        console.print("[bold red]Error[/bold red] closing session: " + str(e))


def verify_password(username):
    good_password = ""
    # print "Enter the %s password to create/modify the OPERS record in UniData." % username
    finished = False
    pw_attempt = 0
    while not finished:
        pw_attempt += 1
        try:
            console.print()
            good_password = getpass(f"Password for {username}@{usertools.DATABASE_SERVER}: ")
            # connect to any account to verify the password works
            session = intercall.Session(usertools.DATABASE_SERVER, usertools.DATABASE_PATHS['production'], retries=1)
            session.connect(username=username, password=good_password)
            finished = session.isOpen()
            session.close()
        except intercall.main.IntercallError:
            pass
        if not finished:
            console.print("[red]Connection failed. Username or password incorrect.[/red]")
            if pw_attempt >= usertools.MAX_PASSWORD_ATTEMPTS:
                raise Exception('Failed to connect to the database')
    return good_password


def create_user(user, options):
    password_used = ""

    # create a session to access UT.OPERS, use the global password
    session = usertools.get_session(UDT_USER, UDT_PASSWORD, options.db_environment, usertools.DATABASE_SERVER)

    okay = False
    count = 1
    while not okay:
        existing_name = usertools.check_opers(user.opersname(), session)
        if existing_name is not None:
            usertools.cs(user=user)
            if count > 1:
                console.print("Attempt " + str(count))
                console.print()
            console.print("[bold yellow]WARNING[/bold yellow]: a record already exists for %s with the name %s." % (user.opersname(), existing_name))
            console.print("")
            result = usertools.prompt("Do you want to use this username?", ['n', 'y', 'q'])
            if result == 'q':
                sys.exit(1)
            elif result == 'n':
                count += 1
                if not user.use_middle:
                    user.use_middle = True
                else:
                    user.alt_username = console.input("Enter an alternate username: ")
            else:
                okay = True
        else:
            okay = True

    usertools.cs(user=user)
    console.print("Creating user %s..." % user.username())

    cmd = f'/usr/sbin/useradd -c "{user.first_last()}" -m -g users {user.username()}'

    (cmd_output, exit_status) = pexpect.run(cmd, withexitstatus=True)
    if exit_status:
        console.print("Exit Status:", exit_status)
        console.print("Command:", cmd)
        console.print("OUTPUT:", cmd_output)
        console.print(usertools.M_ERROR)

        usr_ans = usertools.prompt("Unix account creation failed, continue?", ['n', 'y', 'q'])
        if usr_ans.lower() != "y":
            sys.exit(False)
    else:
        # if we do not have an LDAP user, set the password
        if not user.use_ldap:
            # if the new user was created successfully then we will set
            # the password, but we don't want to on a failure, nor do we
            # want to change the password of an existing user
            console.print("Setting default password...")
            password_used = usertools.set_password(user.username(), options.rand_password)
        else:
            console.print("LDAP users do not have a local password.")
            result = usertools.delete_password(user.username())
            if result is not None:
                console.print(result)

    #
    # An expiration date can be entered, but it must be at least one day
    # ahead.
    #
    if not user.use_ldap:
        expdate = input("Enter an expiration date (ex: 6/1/2005) []: ")
        cmd = '/usr/sbin/usermod -e "%s" %s' % (expdate, user.username())
        (cmd_output, exit_status) = pexpect.run(cmd, withexitstatus=True)
        if exit_status:
            console.print("[bold yellow]WARNING[/bold yellow]: set expiration date failed")
            console.print("OUTPUT:", cmd_output)
    else:
        console.print("LDAP user, skipping expiration...")

    #
    # Specify how long the account can remain inactive (i.e. without the
    # password being changed)
    #
    if not user.use_ldap:
        inactive = input("Specify the maximum number of days the account can be inactive [" + usertools.DEFAULT_INACTIVE + "]:")
        if inactive == "":
            inactive = usertools.DEFAULT_INACTIVE
        cmd = "/usr/sbin/usermod -f %s %s" % (inactive, user.username())
        (cmd_output, exit_status) = pexpect.run(cmd, withexitstatus=True)
        if exit_status:
            console.print("[bold yellow]WARNING[/bold yellow]: set max days failed")
            console.print("OUTPUT:", cmd_output)
    else:
        console.print("LDAP user, skipping inactivity settings...")

    #
    # Policy used to be to lock the production account until Colleague
    # training is complete, and this can be set here.
    #
    usrans = input("Will the user need immediate access to Production [y]:")
    if usrans.lower() == "y" or usrans.strip() == "":
        lock_prod = False
    else:
        lock_prod = True
    
    usertools.cs(True, user)
    #
    # Create the default security classes in the Production OPERS record
    #
    console.print("""
User, %s, has been set up successfully in the OS.

Switch to the Colleague UI client and start the X4NUSR workflow.  Once the
OPERS record for %s has been created you will be prompted to
return to this script.  When you do, this script can add the default
security classes.
""" % (user.username(), user.opersname()))
    usertools.cs(True, user)
    usr_ans = usertools.prompt("Add default security classes?", ['y', 'n'])
    if usr_ans.lower() == "y":
        console.print("Updating security classes...")
        finished = False
        while not finished:
            if usertools.load_default_security(user.opersname(), session, lock_prod):
                console.print("done")
                finished = True
            else:
                console.print("[bold red]ERROR[/bold red]: no UT.OPERS record for " + user.opersname())
                usr_ans = input("Retry update (y/n)? [n]")
                if usr_ans.lower() != "y":
                    finished = True

    usertools.cs(True, user)
    console.print("Now finish adding security classes to the OPERS record in Colleague.")

    usertools.cs(True, user)
    #
    # Copy the OPERS record into selected accounts
    #
    console.print("You can copy the OPERS record to Training once it is finished.\n")
    usr_ans = input("Copy OPERS from %s to Training? [n]" % options.db_environment)
    if usr_ans.lower() == "y":
        console.print(f"Copying {user.opersname()} from {options.db_environment} to training...")
        finished = False
        dest_session = usertools.get_session(UDT_USER,
                                             UDT_PASSWORD,
                                            'training',
                                             usertools.DATABASE_SERVER)
        while not finished:
            if usertools.copy_opers(user.opersname(),
                                    session,
                                    dest_session):
                finished = True
            else:
                console.print("[bold yellow]WARNING[/bold yellow]: copy failed!")
                usr_ans = input("Retry copy (y/n)? [n]")
                if usr_ans.lower() != "y":
                    finished = True
        # now close the destination environment session
        try:
            dest_session.close()
        except intercall.IntercallError as e:
            console.print("[bold red]Error[/bold red] closing session: " + str(e))
        # display the next message about building the SVM record
        usertools.cs(True, user)
        console.print(usertools.M_SVM % (user.first_last(), "Training", user.opersname()))
        console.print("""

Note: If the PERSON record is not found you can add it, just use the College's
address. Create the DMI record in DRUS with the 'Env Operator Equivalent' set
to WEBFACSTAFF.""")

    # close the database session
    try:
        session.close()
    except intercall.IntercallError as e:
        console.print("[bold red]Error[/bold red] closing session: " + str(e))

    usertools.cs(True, user)
    console.print("User creation complete.")
    console.print("-" * 79)
    console.print("Username: " + user.username())
    console.print("OPERS:    " + user.opersname())
    if not user.use_ldap:
        console.print("Password: " + password_used)
    console.print("-" * 79)


def change_name(session, old_username, user, use_ldap, use_log):
    """Return True if the user's name was changed in the system"""
    success = False

    if use_log:
        logger1 = logging.getLogger('coluser.changeName')
    else:
        logger1 = None

    if user.username().strip() == "":
        msg = "New username is blank for %s, skipped..." % old_username
        if use_log:
            logger1.error(msg)
        else:
            console.print("[bold red]ERROR[/bold red]:", msg)
        return success
    
    cmd = 'groups %s' % old_username

    cmd_output = pexpect.run(cmd)

    groups = cmd_output.decode('utf-8').strip().split(':')[1].strip().split(' ')[1:]

    # make sure we have an employee number
    if use_log:
        logger1.debug('(before) person_id = %s', user.person_id)
    else:
        console.print("[DEBUG] person_id =", user.person_id)
    if user.person_id is None:
        user.get_employee_id_from_database(session)
        if use_log:
            logger1.debug('(after) person_id = %s', user.person_id)
        else:
            console.print("[DEBUG] (after) person_id =", user.person_id)
        # now that we have found the employee number we can build the
        # username information from the database
        user.get_name_from_database(user.person_id, use_ldap, session)

    if len(groups) > 0:
        other_groups = "-G " + ",".join(groups)
    else:
        other_groups = ""
    cmd = f'usermod -l {user.username()} -c "{user.first_last()}" -d /home/{user.username()} -m {other_groups} {old_username}'

    msg = f"Changing OS account from {old_username} to {user.username()}"
    if use_log:
        logger1.info(msg)
        logger1.debug("cmd = %s", cmd)
    else:
        console.print(msg)

    (cmd_output, exit_status) = pexpect.run(cmd, withexitstatus=True)

    if exit_status:
        if use_log:
            logger1.error("usermod failed, %s", cmd_output)
        else:
            console.print("[bold red]ERROR[/bold red]: usermod failed")
            console.print("OUTPUT:", cmd_output)
    else:
        # remove the password settings and lock the local account
        result = usertools.delete_password(user.username())
        if result is not None:
            if use_log:
                logger1.error(result)
            else:
                console.print(result)
        success = True
    return success


def change_colleague(udt_user, udt_passwd, env_name, db_host, old_username, user, use_log=False):
    if use_log:
        logger1 = logging.getLogger('coluser.changeColleague')
    else:
        logger1 = None
    #
    # Make sure the new username is not blank
    #
    if user.opersname().strip() == "":
        msg = "New username is blank for %s, skipping..." % old_username
        if use_log:
            logger1.warning(msg)
        else:
            console.print("[bold yellow]WARNING[/bold yellow]:", msg)
        return
    #
    # if we're changing prod, then change all the other environments too
    #
    if env_name == 'production':
        which_env = usertools.DATABASE_PATHS.keys()
    else:
        which_env = [env_name]
    for db_env in which_env:
        this_session = usertools.get_session(udt_user, udt_passwd, db_env, db_host)
        rename_records(this_session, old_username, user, use_log)
        try:
            this_session.close()
        except intercall.IntercallError as e:
            console.print("[bold red]Error[/bold red] closing session: " + str(e))


def rename_records(session, old_username, user, use_log=False):
    """Modify the database records that use the username as the key"""
    if use_log:
        logger1 = logging.getLogger('coluser.renameRecords')
    else:
        logger1 = None
    #
    # Change the OPERS record to the new username
    #
    old_opersname = old_username.upper()
    msg = "Now changing Colleague records in the " + session.name() + " environment..."
    if use_log:
        logger1.info(msg)
    else:
        console.print(msg)

    ut_opers = usertools.UtOpers(session)
    staff = usertools.Staff(session)

    # change the username in UT.OPERS
    try:
        rec = ut_opers.get(old_opersname)
        msg = "Changing the OPERS record for " + rec.sys_user_name + ", from " + old_opersname + " to " + user.opersname()
        if use_log:
            logger1.info(msg)
        else:
            console.print()
            console.print(msg)
        rec.sys_user_name = user.first_last()
        rec.sys_user_last_device_used = user.opersname()
        rec.opers_chgopr = 'DATATEL'
        rec.opers_chgdate = datetime.date.today().strftime("%m/%d/%Y")
        #
        # Now we need to rename the record
        #
        cmd = 'COPY FROM UT.OPERS %s,%s' % (old_opersname,
                                            user.opersname())
        result = session.execute(cmd)
        if use_log:
            logger1.info(result)
        else:
            console.print('[bold cyan]INFO[/bold cyan]', result)
        if result.return_code[0] > 0:
            ut_opers.delete(old_opersname)
        else:
            if use_log:
                logger1.error('UT.OPERS copy failed')
            else:
                console.print("[bold red]ERROR[/bold red] UT.OPERS copy failed")
    except Exception as val:
        if use_log:
            logger1.error('failed to copy OPERS record: %s', str(val))
        else:
            console.print(val)
    # Update the username in STAFF (which updates SVM)
    try:
        rec = staff.get(user.person_id)
        rec.staff_login_id = user.opersname()
    except Exception as val:
        if use_log:
            logger1.error('failed to rename STAFF record', exc_info=True)
        else:
            console.print(val)
    # Copy the UI settings and rename any GL security
    # records
    for ui_filename in COLLEAGUE_PREFS:
        msg = 'looking for records in %s...' % ui_filename
        if use_log:
            logger1.info(msg)
        else:
            console.print('[bold cyan]INFO[/bold cyan]]', msg)
        cmd = 'SELECT %s WITH @ID LIKE "%s..." TO 1' % (ui_filename, old_opersname)
        try:
            result = session.execute(cmd)
            if use_log:
                logger1.info(result)
            else:
                console.print('[bold cyan]INFO[/bold cyan]', result)
            for rec_id in session.readNext(1):
                if '*' in rec_id:
                    username, rec_type = rec_id.split('*')
                    new_id = "%s*%s" % (user.opersname(), rec_type)
                    cmd = 'CNAME %s %s,%s' % (ui_filename, rec_id, new_id)
                else:
                    cmd = 'CNAME %s %s,%s' % (ui_filename, old_opersname, user.opersname())
                if use_log:
                    logger1.debug(cmd)
                else:
                    console.print('[DEBUG]', cmd)
                result = session.execute(cmd)
                if use_log:
                    logger1.info(str(result).strip())
                else:
                    console.print('[bold cyan]INFO[/bold cyan]', str(result).strip())
        except Exception as val:
            if use_log:
                logger1.error('CNAME failed for %s', ui_filename, exc_info=True)
            else:
                console.print(val)


if __name__ == "__main__":
    main()
