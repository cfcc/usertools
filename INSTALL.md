# Installation Instructions

These scripts depend on 2 python libraries:

 * pexpect (https://github.com/pexpect/pexpect)
 * intercall (https://bitbucket.org/cfcc/python-intercall2/)

If you are not installing the libraries system-wide, then you will need
to create a ./lib directory and put the files there.

## Installing in a Virtual Environment

With Python 3 the best way to install this program is with a virtual
environment.  That will keep the dependencies limited to a single
directory tree.

 1. Create the main directory
 
        mkdir /opt/usertools

 3. Download the source from Bitbucket

        cd /opt
        git clone git@bitbucket.org:cfcc/usertools.git

 4. Create the virtual environment

        cd /opt/usertools
        python3 -m venv venv

 5. Activate the virtual environment and install requirements

        source venv/bin/activate
        pip install -r requirements.txt

 6. Install the usertools modules in the virtual environment

        pip install -e .
