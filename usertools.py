"""Python Module with routines for modifying user accounts.

Part of the coluser script.

"""

import calendar
import datetime
import platform

import pexpect
import re
import shutil
import time

import intercall
from console import console
from rich.columns import Columns
from rich.console import Text
from rich.padding import Padding
from rich.panel import Panel

#
# DEFAULT SETTINGS
#
DEFAULT_PASSWORD = 'set2go'
DEFAULT_INACTIVE = "91"  # 13 weeks (to comply with PCI standards)
DEFAULT_TIMEOUT = '1800'
DEFAULT_SECURITY_CLASSES = ["CA.LOONLY",  # UA.LOONLY is added by the workflow
                            "FA.LOONLY",
                            "SA.LOONLY",
                            "HA.LOONLY",
                            "DENY_SEARCH_EXPORT",
                            "DENY_WINDOWS_EXPORT",
                            "CA.CUSTTAB",
                            "CADM.SSNDENY"]
DEFAULT_OPERS_PASSWORD = "NOT4UYET"
DATABASE_SERVER = "localhost"  # make sure the changes happen on the same server that has the database
DATABASE_PATHS = {
    "production": "/datatel/coll18/production/apphome",
    "test": "/datatel/coll18/test/apphome",
    "test1": "/datatel/coll18/test1/apphome",
    "test2": "/datatel/coll18/test2/apphome",
    "dev": "/datatel/coll18/dev/apphome",
}
UMASK = '0027'  # *FIXME* this should apply somewhere when we execute commands :-/
MAX_PASSWORD_ATTEMPTS = 3

M_ERROR = "Abnormal exit"
FTPUSERS_PATH = "/etc/ftpd/ftpusers"

# these are prompt strings for pexpect to use when matching
COLON_PROMPT = r'\:$'
AE_PROMPT = r'\*\-\-\: $'

hostname = pexpect.run("uname -n").strip().upper()

PURPOSE = ("CREATE COLLEAGUE USER",
           "MODIFY COLLEAGUE USER",
           "COPY COLLEAGUE USER")
HOSTNAME = hostname.decode('utf8')
HEADING = PURPOSE[0]
# HEADING = "CREATE COLLEAGUE USER" + " " * 39 + "%20s" % hostname.decode('utf8')
WELCOME = """At each prompt enter an appropriate response, the default is the value in the brackets.  Use CTRL+C to exit prematurely.
"""
M_SVM = """Create a Staff record (CORE -> SVM) for %s in the %s
environment.

 * Staff Code: %s
 * Employee Type: E
 * Staff Status: C
 * Privacy Access: D
 """


def set_db_paths(new_paths):
    """Update the module's list of database paths.

    new_paths - a list of tuples with the environment name and path
    
    Uses the information from the config file, for example:

        usertools.setDbPaths(config.items('Environment'))

    """
    global DATABASE_PATHS
    assert (isinstance(new_paths, list))
    for entry in new_paths:
        assert (isinstance(entry, tuple))
        DATABASE_PATHS[entry[0]] = entry[1]


def get_session(username, password, environment, server):
    """Return a Intercall session object for the Colleague environment"""
    session = intercall.Session(server, DATABASE_PATHS[environment], retries=1)
    session.connect(username, password)
    return session


def in_modify_mode(which=1):
    global HEADING
    # HEADING = "MODIFY" + HEADING[6:]
    HEADING = PURPOSE[which]


def cs(wait_before=False, user=None):
    """Clear the screen.
    
    If wait_before is true, then the screen will be cleared once the user
    presses enter and a header will be printed.
    
    """
    if wait_before:
        console.input("Press <ENTER> to continue...")
    # sys.stdout.write(os.popen('clear').read())
    console.clear()
    my_text = (Text(HEADING), Text(HOSTNAME, justify="right"))
    console.print(Padding(Columns(my_text, expand=True), (1, 1), style="on blue"))
    if user is not None:
        user_panel = Panel("USERNAME............  PERSON ID\n"
                           "%-20s  %7s" % (user.username(), user.person_id))
        console.print(user_panel)

    # print('-' * 80)
    console.print()


def prompt(msg, valid_reply=None, use_default=True):
    """Return an input from the user, limited to a valid list.

    The first item of the valid_reply list is the default answer if
    the use_default flag is set.  Otherwise, the user must enter
    something.  If you want to allow 'q' for quit, it must be in the
    valid_reply list.

    An empty list will disable checking.

    """
    reply = None
    finished = False
    while not finished:
        if valid_reply is not None:
            if not isinstance(valid_reply, list):
                raise Exception("valid_reply parameter must be a list")
            usr_ans = console.input(msg + r' \[' + valid_reply[0] + '] ')
            if usr_ans in valid_reply:
                reply = usr_ans
                finished = True
            elif usr_ans.strip() == "" and use_default:
                reply = valid_reply[0]
                finished = True
            else:
                console.print(r'Reply must match one of \[' + ','.join(valid_reply) + ']')
                console.print()
        else:
            usr_ans = input(msg)
            reply = usr_ans
            finished = True
    return reply


class UsernameError(Exception):
    """
    To be raised when the username entered fails the formatting checks.
    """

    def __init__(self, val):
        Exception.__init__(self, val)
        self.val = val

    def __str__(self):
        return "Name must match First [Middle] Last: " + str(self.val)


# create an Intercall schema for the OPERS file
class UtOpers(intercall.UniFile):
    ic_filename = "UT.OPERS"
    sys_user_password = intercall.StringCol(loc=2, control='E')
    sys_user_classes = intercall.StringCol(loc=4, multivalued=True, control='E')
    sys_user_last_device_used = intercall.StringCol(loc=9, control='E')
    sys_person_id = intercall.StringCol(loc=15, control='E')
    sys_user_name = intercall.StringCol(loc=16, control='E')
    sys_user_timeout_override = intercall.StringCol(loc=39, mv=True, control='E')
    sys_user_menu_interface = intercall.StringCol(loc=40, mv=True, control='E')
    sys_user_menu_process = intercall.StringCol(loc=41, mv=True, control='E')
    opers_addopr = intercall.StringCol(loc=42, control='E')
    opers_adddate = intercall.DateCol(loc=43, control='E')
    opers_chgopr = intercall.StringCol(loc=44, control='E')
    opers_chgdate = intercall.DateCol(loc=45, control='E')


class Staff(intercall.UniFile):
    ic_filename = "STAFF"
    staff_initials = intercall.StringCol(loc=1)
    staff_letters = intercall.StringCol(loc=2)
    staff_comments = intercall.StringCol(loc=3)
    staff_type = intercall.StringCol(loc=4)
    staff_status = intercall.StringCol(loc=5)
    staff_office_code = intercall.StringCol(loc=6)
    staff_login_id = intercall.StringCol(loc=21)
    person = intercall.ForeignKey(0, 'Person')
    person_pin = intercall.ForeignKey(0, 'PersonPin')


class Person(intercall.UniFile):
    ic_filename = "PERSON"
    last_name = intercall.StringCol(loc=1, len=25, immutable=True)
    first_name = intercall.StringCol(loc=3, len=15, immutable=True)
    middle_name = intercall.StringCol(loc=4, len=15, immutable=True)
    birth_date = intercall.DateCol(loc=14, immutable=True)
    nickname = intercall.StringCol(loc=22, immutable=True)
    preferred_name = intercall.StringCol(loc=15, immutable=True)
    person_user4 = intercall.StringCol(loc=98, immutable=True)
    person_email_types = intercall.StringCol(loc=67, len=3, mv=True, immutable=True)
    person_email_addresses = intercall.StringCol(loc=68, len=50, mv=True, immutable=True)
    person_preferred_email = intercall.StringCol(loc=219, len=1, mv=True, immutable=True)
    name_history_last_name = intercall.StringCol(loc=44, immutable=True)
    name_history_first_name = intercall.StringCol(loc=45, immutable=True)
    name_history_middle_name = intercall.StringCol(loc=46, immutable=True)


class PersonPin(intercall.UniFile):
    ic_filename = "PERSON.PIN"
    person_id = intercall.StringCol(loc=0, len=7, dbname='@ID')
    user_id = intercall.StringCol(loc=8, len=60, dbname="PERSON.PIN.USER.ID")


class User:
    """Encapsulate the username information.

    A username can either be a first initial and the last name or if
    that already is being used by someone else, then put in the middle
    initial and reduce the last name to 6 characters.

    """

    def __init__(self):
        """Create a blank name"""
        self.person_id = None

        self.first = ""
        self.middle = ""
        self.last = ""

        self.ldap_username = None
        self.alt_username = None

        self.use_middle = False
        self.use_ldap = False

    def parse(self, fullname):
        """Parse the name in to component parts, or throw UsernameError"""
        parts = fullname.split(" ")
        if len(parts) == 3:
            self.first = parts[0]
            self.middle = parts[1]
            self.last = parts[2]
        elif len(parts) == 2:
            self.first = parts[0]
            self.last = parts[1]
        else:
            raise UsernameError(fullname)

    def get_name_from_database(self, person_id, use_ldap, session):
        """Get the name from the UniData record."""
        self.person_id = person_id
        self.use_ldap = use_ldap
        # set up the needed files
        intercall.register_session(session)

        rec = Person.get(self.person_id)
        self.first = rec.first_name
        self.middle = rec.middle_name
        self.last = rec.last_name

        rec = PersonPin.get(self.person_id)
        self.ldap_username = rec.user_id

    def get_employee_id_from_database(self, session):
        """Set the employee number from UT.OPERS if the username is found.
        Requires that the alt_username has been set.
        
        session - an Intercall session object

        """
        assert self.alt_username is not None, "alt_username cannot be blank to get Employee ID from UniData"
        intercall.register_session(session)
        rec = UtOpers.get(self.alt_username.upper())
        self.person_id = rec.sys_person_id

    def set_username(self, n):
        """Set an alternate username"""
        self.alt_username = n

    def username(self):
        """Return the generated username"""
        un = ""
        if self.alt_username is None:
            if self.use_ldap:
                un = self.ldap_username
            else:
                if self.use_middle and self.middle != "":
                    un = self.first[0].lower() + self.middle[0].lower() + self.last.lower().strip("-")[:6]
                else:
                    un = self.first[0].lower() + self.last.lower().strip("-")[:7]
        else:
            un = self.alt_username
        return un

    def opersname(self):
        """The opers name is all uppercase"""
        u_name = self.username()
        if u_name is not None:
            s_out = u_name.upper()
        else:
            s_out = ""
        return s_out

    def first_last(self):
        """Just the first and last name for the gecos field"""
        return self.first + " " + self.last


def get_printer_groups():
    re_groupname = re.compile(r'^(\w+?ptr):')
    printer_groups = list()
    with open("/etc/group") as fd:
        for line in fd:
            result = re_groupname.search(line)
            if result is not None:
                printer_groups.append(result.group(1))
    printer_groups.sort()
    return printer_groups


def check_opers(opersname, session):
    """Return a name if the opers record already exists for the user or None"""
    intercall.register_session(session)
    name = None
    try:
        rec = UtOpers.get(opersname)
        name = rec.sys_user_name
    except Exception as val:
        console.print(val)
    return name


def load_default_security(opersname, session, do_lock=True):
    """Return true if the default security classes are added
    
    Edit the UT.OPERS record and add additional default security in
    field 4.  If this is a new record (i.e. no field 4), then we do
    not want to add anything.

    UPDATE: now that the timeout settings are important, we make sure
    those are set too.

    Parameters:
        opersname -- the record ID for UT.OPERS
        environment -- the key for DATABASE_PATHS
        do_lock -- set an Envision password on the Production record
    
    """
    success = False
    try:
        intercall.register_session(session)
        rec = UtOpers.get(opersname)
        # Add the security classes
        curr_sec = rec.sys_user_classes[:]
        curr_sec.extend(DEFAULT_SECURITY_CLASSES)
        rec.sys_user_classes = curr_sec
        # Update the timeout settings
        rec.sys_user_timeout_override = [DEFAULT_TIMEOUT, DEFAULT_TIMEOUT]
        rec.sys_user_menu_interface = ['UI', 'WEB']
        rec.sys_user_menu_process = ['*']
        if do_lock:
            rec.sys_user_password = DEFAULT_OPERS_PASSWORD
        success = True
    except intercall.IntercallError as e:
        console.print("[bold red]Error[/bold red] in load_default_security(): " + str(e))
    return success


def checkFtpUsers(username, needs_access=False):
    """
    Load the ftpusers file and verify that the user has the correct access.

    The default is to add the username (if not present) and deny access.  If
    the 'needs_access' parameter is set to true, then the username will be
    removed from the file (if present) so access will be granted.

    """
    if platform.system() == 'Solaris':
        placeholder = "# regular users follow"
        header = ""
        on_header = True
        usernames = dict()
        print("Checking the ftpusers file...", end=' ')
        # make a copy just in case
        shutil.copy(FTPUSERS_PATH, FTPUSERS_PATH + "~")
        try:
            fd = open(FTPUSERS_PATH, 'rt')
            for line in fd:
                if line.strip() == placeholder:
                    on_header = False
                    header += line
                    continue
                if on_header:
                    header += line
                else:
                    usernames[line.strip()] = 1
            fd.close()

            if username not in usernames:
                if not needs_access:
                    print(" blocking access...", end=' ')
                    usernames[username] = 1
                else:
                    print(" up to date...", end=' ')
            else:
                # if the username is already present then we'll check if they
                # should actually have access to FTP, and if so remove them.
                if needs_access:
                    print(" allowing access...", end=' ')
                    del usernames[username]
                else:
                    print(" up to date...", end=' ')

            # now sort the list and save the changes
            text_out = header + "\n".join(sorted(usernames.keys())) + "\n"
            fd = open(FTPUSERS_PATH, 'wt')
            fd.write(text_out)
            fd.close()
            print(" done")
        except Exception as e:
            print(" failed")
            print(str(e))
    else:
        console.print("/etc/ftpd/ftpusers not supported on this platform.")


def set_password(username, use_random=True, new_password=None):
    """Returns the password used or None on an error"""
    result = None
    if use_random:
        d = datetime.date.fromtimestamp(time.time())
        dn = calendar.day_name[calendar.weekday(d.year, d.month, d.day)]
        pregen_passwd = dn.lower()[:3] + str(d.year)
    elif new_password is not None:
        pregen_passwd = new_password
    else:
        pregen_passwd = DEFAULT_PASSWORD

    child = pexpect.spawn('passwd ' + username)
    i = child.expect(['New Password: ', 'passwd: User unknown: ', 'Permission denied'])
    if i == 0:
        child.sendline(pregen_passwd)
        child.expect('Re-enter new Password: ')
        child.sendline(pregen_passwd)
        child.expect('passwd: password successfully changed for ' + username)
        (cmd_output, exit_status) = pexpect.run("passwd -f " + username,
                                                withexitstatus=True)
        console.print(cmd_output)
        if exit_status:
            console.print("[bold yellow]WARNING[/bold yellow]: set password expiration failed")
            console.print("OUTPUT:", cmd_output)
        else:
            result = pregen_passwd
    else:
        console.print("[bold red]ERROR[/bold red]", child.after)
    return result


def delete_password(username):
    """Turn off password aging and lock the local password on the account so only Kerberos works"""
    result = None
    (cmd_output, exit_status) = pexpect.run("chage -M -1 " + username,
                                            withexitstatus=True)
    if exit_status:
        result = cmd_output
    (cmd_output, exit_status) = pexpect.run("passwd -l " + username,
                                            withexitstatus=True)
    if exit_status:
        result += cmd_output
    return result


def copy_opers(name, src_env_sess, dest_env_sess):
    """Return true if the OPERS record was copied to the dest. env.
    
    Parameters:

        name -- the OPERS record key

        src_env_sess -- an Intercall session for source environment

        dest_env_sess -- the destination environment session
    
    To make the copy between environments (basically UniData
    databases), we read the whole record from the source environment
    and then write it back to the other environment.
    
    """
    success = False
    opers_rec = None
    if src_env_sess.name() == dest_env_sess.name():
        console.print("[bold red]ERROR[/bold red]: source and destination cannot be the same!")
        return False
    try:
        intercall.register_session(src_env_sess)
        ut_opers = UtOpers.get(name)
        opers_rec = ut_opers.recordRead(name)
    except intercall.IntercallError as e:
        console.print("copyOpers error: " + str(e))

    if opers_rec is not None:
        try:
            try:
                intercall.register_session(dest_env_sess)
                # First write the data from the other environment
                ut_opers = UtOpers.get(name)
                ut_opers.recordWrite(name, opers_rec)
                # Then clear out the system fields
                rec = UtOpers.get(name)
                rec.sys_person_id = ""
                rec.sys_user_password = ""
                success = True
            except IOError as e:
                console.print("[bold red]Error[/bold red] writing record to UT.OPERS in the training environment")
                console.print(e)
        except intercall.IntercallError as e:
            console.print("copyOpers error: " + str(e))
    return success
