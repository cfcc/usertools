#!/usr/bin/env python

"""Convert Unix user accounts to Active Directory user accounts.

Requires the coluser script/modules which in turn require:

 * pexpect
 * python-intercall

Everything else should be built-in to Python.

--- IMPORTANT ---

You must run 'configure.py' to set up the connection information
before running this script.

"""
import csv
import configparser
import logging
import logging.config
from optparse import OptionParser
import os
import os.path
import sys

# NOTE: this is just for portable installations, don't create it if
#       you installed the required modules system-wide.  We'll look
#       for a site-packages location and a simple 'lib' directory.
ver_str = "python%d.%d" % (sys.version_info[0], sys.version_info[1])
for lib_dir in [os.path.join('.', 'lib', ver_str, 'site-packages'),
                os.path.join('.', 'lib')]:
    if os.access(lib_dir, os.F_OK):
        sys.path.append(lib_dir)

import coluser
import usertools

import configure

USAGE = "%prog [options] [OLD_USERNAME NEW_USERNAME]"

USE_LDAP = True
USE_LOG = True
DB_ENVIRONMENT = 'production'

UDT_USER = "datatel"
UDT_PASSWORD = ""

IMPT_OLD_NAME_POS = 0
IMPT_NEW_NAME_POS = 1

EXCLUDED_USERS = ['datatel', 'webwiz']

OLD_USERNAME = ""
NEW_USERNAME = ""


def main():
    global UDT_USER
    global UDT_PASSWORD
    global DB_ENVIRONMENT
    global USE_LOG
    
    logging.config.fileConfig('logging.conf')
    logger1 = logging.getLogger('coluser.convert.main')

    config = configparser.SafeConfigParser()
    config.read("unidata.cfg")

    parser = OptionParser(usage=USAGE)
    parser.add_option('-D', '--debug', dest="debug",
                      action="store_true", default=False,
                      help="Debug mode, no updates made to the accounts")
    parser.add_option('-E', '--environment', dest='col_env', default=None,
                      help="Specify a single Colleague Environment (default=%s)" % DB_ENVIRONMENT)
    parser.add_option('-f', '--file', dest="import_fn", default=None,
                      help="Specify an import CSV file with old and new usernames")
    parser.add_option('-p', '--password-only', dest="force_password_delete",
                      action="store_true", default=False,
                      help="Delete the password on previously converted accounts.")
    parser.add_option('--colleague-only', dest='colleague_only',
                      action="store_true", default=False,
                      help="Rename the Colleague records only.")

    (options, args) = parser.parse_args()

    if options.import_fn is None:
        if len(args) > 1:
            OLD_USERNAME = args[0]
            NEW_USERNAME = args[1]
        else:
            parser.error("Converting a single user requires both the old and new usernames")

    if options.col_env is not None:
        DB_ENVIRONMENT = options.col_env

    if config.has_option('Connection', 'db_user'):
        UDT_USER = config.get('Connection', 'db_user')

    if config.has_section('Environment'):
        usertools.set_db_paths(config.items('Environment'))

    if config.has_section('Users'):
        if config.has_option('Users', 'excluded'):
            EXCLUDED_USERS = config.get('Users', 'excluded')
            if not isinstance(EXCLUDED_USERS, list):
                EXCLUDED_USERS = [s.strip() for s in EXCLUDED_USERS.split(',')]

    if options.debug:
        print("[DEBUG] " + repr(usertools.DATABASE_PATHS))

    # get the password for connecting to the database
    if config.has_option('Connection', 'db_password'):    
        UDT_PASSWORD = configure.get_password(config.get('Connection', 'db_password'))
    else:
        UDT_PASSWORD = coluser.verify_password(coluser.UDT_USER)

    udt_session = usertools.get_session(UDT_USER, UDT_PASSWORD, DB_ENVIRONMENT, usertools.DATABASE_SERVER)

    print("")
    print("[STATUS] converting user accounts...")

    converted_ids = []
    
    if options.import_fn is not None:
        # load the users from an Informer report exported as a Tab-Delimited file
        with open(os.path.expanduser(options.import_fn), 'rb') as fd:
            dialect = csv.Sniffer().sniff(fd.read(1024))
            fd.seek(0)
            reader = csv.reader(fd, dialect)

            for row in reader:
                success = rename(row[IMPT_OLD_NAME_POS],
                                 row[IMPT_NEW_NAME_POS],
                                 udt_session,
                                 options)
                if success:
                    converted_ids.append(row[IMPT_NEW_NAME_POS])
    else:
        success = rename(OLD_USERNAME, NEW_USERNAME, udt_session, options)
        if success:
            converted_ids.append(NEW_USERNAME)

    print("-" * 79)
    print("\nThe following records were converted successfully:\n")
    print("-" * 79)
    print()
    print("\n".join(converted_ids))
    print()


def rename(old_username, new_username, udt_session, options):
    """Return true if the account was renamed at the Unix level.

    As part of the rename we update UT.OPERS and SVM in UniData.

    """
    success = False
    logger1 = logging.getLogger('coluser.convert.rename')

    # make sure we don't convert certain users, such as datatel
    if old_username in EXCLUDED_USERS:
        print("[STATUS] skipping %s, should not be converted" % old_username)
    else:
        msg = "starting conversion of %s..." % old_username
        print("[STATUS]", msg)
        logger1.info(msg)
        try:
            user = usertools.User()

            # use the old username to fetch the person ID and then
            # switch it to the new name
            user.set_username(old_username)

            user.get_employee_id_from_database(udt_session)

            user.set_username(new_username)
            
            if not options.debug:
                if options.colleague_only:
                    print("[STATUS] updating Colleague only for %s" % old_username)
                    coluser.change_colleague(UDT_USER,
                                             UDT_PASSWORD,
                                             DB_ENVIRONMENT,
                                             usertools.DATABASE_SERVER,
                                             old_username,
                                             user,
                                             USE_LOG)
                    success = True
                else:
                    if options.force_password_delete:
                        print("[STATUS] only deleting password for %s" % old_username)
                        success = True
                    elif old_username != user.username():
                        print("[STATUS] Converting %s to %s" % (old_username, user.username()))

                        # The function will return True on sucess so we can
                        # make a list of the users who were successfully converted.
                        success = coluser.change_name(udt_session,
                                                      old_username,
                                                      user,
                                                      USE_LDAP,
                                                      USE_LOG)
                        if success:
                            coluser.change_colleague(UDT_USER,
                                                     UDT_PASSWORD,
                                                     DB_ENVIRONMENT,
                                                     usertools.DATABASE_SERVER,
                                                     old_username,
                                                     user,
                                                     USE_LOG)
                    else:
                        print("[STATUS] account for %s has already been converted" % old_username)
                        success = False
                    if success:
                        # Remove the Solaris password information
                        result = usertools.delete_password(user.username())
                        if result is not None:
                            logger1.error(result)
                if success:
                    # update the list of successful conversion
                    logger1.info("Conversion of %s (%s) successful", old_username, user.person_id)
                else:
                    logger1.info("Conversion of %s failed", old_username)
            else:
                if user.username().strip() != "":
                    print("[DEBUG] Would convert %s (%s) to %s" % (old_username, user.person_id, user.username()))
                else:
                    print("[DEBUG] Username is blank for %s, skipped" % old_username)
        except Exception as ex:
            logger1.error("failed on %s: %s", old_username, ex)
            logger1.error(ex, exc_info=True)
    return success


if __name__ == '__main__':
    main()
