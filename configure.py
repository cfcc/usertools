#!/usr/bin/env python3

import configparser
from getpass import getpass
import bz2
import base64

CONFIG_FN = 'unidata.cfg'


def main():
    db_path = ""
    db_user = ""
    db_password = ""

    config = configparser.ConfigParser()
    config.read(CONFIG_FN)

    if not config.has_section('Connection'):
        config.add_section("Connection")

    # request a username to login to the database
    if config.has_option('Connection', 'db_user'):
        default = config.get('Connection', 'db_user')
    else:
        default = ""
    db_user = input(f"Enter database user [{default}]: ")
    if db_user.strip() == "":
        db_user = default
    # get the password for the username
    if config.has_option('Connection', 'db_password'):
        default = config.get('Connection', 'db_password')
        password_display = "********"
    else:
        default = None
        password_display = ""
    finished = False
    while not finished:
        password_raw = getpass(f"Enter password for {db_user} [{password_display}]: ")
        if password_raw == "":
            if default is not None:
                db_password = default
                finished = True
            else:
                print("Passwords cannot be blank")
        else:
            password_check = getpass("Verify password: ")
            if password_raw == password_check:
                db_password = encode_password(password_raw)
                finished = True
            else:
                print("The passwords did not match")

    config.set('Connection', 'db_password', db_password)
    config.set('Connection', 'db_user', db_user)

    if not config.has_section('Environment'):
        config.add_section("Environment")
    else:
        print("\nCurrently configured environments:")
        for env_name in sorted(config.options('Environment')):
            print("\t%s: %s" % (env_name, config.get('Environment', env_name)))
        print()

    finished = False
    while not finished:
        which = input("Enter environment name (ex: production) [Enter to finish]: ")
        if which.strip() == "":
            finished = True
        else:
            if config.has_option('Environment', which):
                default = config.get('Environment', which)
            else:
                default = ''
            db_path = input(f"Enter '{which}' database path [{default}]: ")
            if db_path.strip() != "":
                config.set('Environment', which, db_path)
            else:
                if default != "":
                    config.set('Environment', which, default)
                else:
                    print("Cannot add blank paths")

    if not config.has_section('Users'):
        config.add_section("Users")
    if config.has_option('Users', 'excluded'):
        default = config.get('Users', 'excluded')
    else:
        default = ''
    print(f"\nEnter a list of users to exclude separated by commas, none for an empty list,\nor ENTER to keep the current setting\n\t(default={default})\n")
    usr_ans = input("Users to exclude: ")
    if usr_ans.lower() == 'none':
        excluded_users = ""
    elif usr_ans != "":
        excluded_users = usr_ans
    else:
        excluded_users = default
    config.set('Users', 'excluded', excluded_users)

    with open(CONFIG_FN, 'w') as configfile:
        config.write(configfile)
        print("Wrote file:", CONFIG_FN)


def encode_password(plaintext):
    """Return a base64 encoded string.

    The plaintext is obfuscated with bzip2 and converted to base64.
    
    """
    ciphertext = bz2.compress(plaintext.encode('utf8'))
    return base64.b64encode(ciphertext).decode('utf8')


def get_password(encoded_str):
    """Return the plaintext result of the encoded/encrypted string.

    The header is stripped from the string before base64 decoding.

    """
    ciphertext = base64.b64decode(encoded_str)
    plaintext = bz2.decompress(ciphertext)
    return plaintext


if __name__ == "__main__":
    main()
